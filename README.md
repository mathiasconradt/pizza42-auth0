Pizza 42
========

Auth0 demo application. 

A working example is available on Heroku at https://evening-meadow-75414.herokuapp.com/ as well as https://pizza42.techtical.io (hosted on a RaspberryPi3 in my home office; in case the latter is unavailable, please use the Heroku instance).

Architecture and development approach:
======================================
I chose to put SPA and backend into one application and on one host/subdomain for simplicity reasons, so only one subdomain and port is needed.
The SPA is based on Javascript. The backend is based on Spring Boot, though it also hosts the SPA.

User profiles including meta data (google connection count) reside at Auth0.
In a real world, Pizza42 would most likely have a separate database as well for orders, etc., but for this demo, it wasn't needed.

Requirements Fulfilment:
========================

(1) 
*Show how a new customer can sign up and an existing customer can sign in with
email/password or Google*

In the Pizza42 app settings in Auth0 management console, both database (username/password) as well as social login with Google is enabled.

(2)
*Ensure that if a customer signs in with either an email/password or Google, it will be
 treated as the same user if they use the same email address*
 
 This is handled by applying the rule `Link Accounts with Same Email Address while Merging Metadata` in the Auth0 rules settings in the management console.
 
 (3)
 *Show that the solution can be built as a “modern” web application (SPA) which can then
  securely call the API backend of Pizza 42 using OAuth.*
  
SPA is based on Javascript, API backend is based on Spring Boot. For security purposes, all Auth0 management API calls are made from the API backend, not from the SPA directly, otherwise we would need to expose the management API token to the client.

(4)
*Require that a customer has a verified email address before they can place a pizza
order, but they should still be able to sign into the app .*  

The `email_verified` status is checked from the user profile. The flag is included in the user profile obtained by the user as well as by the full user profile obtained by the management API.
If the email is verified, the order button in the SPA is enabled, however an additional check is also done on the backend side when the user tries to submit an order.

(5)
*Use Auth0 features to gather additional information about a user (specifically their
 gender) without prompting them directly.*

The gender, if available, is fetched from the full user profile obtained by the Auth0 management API. If male or female, a respective title (Mr. / Ms.) is used for the user greeting on the SPA.

(6)
*Use Auth0 to call the Google People API to fetch the total number of Google connections a user has and store that count in their user profile.*

When the user authenticates via Google account, we get an access token from Google, which we can use for calling the Google People API on behalf of the user. Note that in this demo, the number of connections is only fetched once (at first time when the user authenticates via Google account), not everytime the user returns to the app. Otherwise, refresh token handling would need to be implemented.
The number of Google connections is stored in the `user_metadata` of the user profile on Auth0 side.

![Google Connection stored in user profile](docs/auth0_google_connections_store.png)


Any difficulties during development?
============================
During the development, I mainly used the Auth0 documentation and Google API docs as main sources.

For the SPA, I used the tutorial/getting started apps based on Javascript. 
For the backend, I used a plain Spring boot project from scratch.

There were no major issues during development, only:

* had to check the API docs of getting a user profile via auth API vs. management API. Learned that obviously not all information (such as gender, full name, all identities) can be fetched by a user but only via management API. 
* `java.net.HttpURLConnection` of JDK does not support http `PATCH` method, so I had to add it via reflection.


Development / Deployment Guide:
================

System Requirements: JDK 8+, Gradle 3.4.1

The project **sources** can be cloned from the bitbucket repository:

`git clone https://bitbucket.org/mathiasconradt/pizza42-auth0`

- Make sure to adjust the `application.properties` (use `src/main/resources/application.properties.example` as basis) regarding the correct value for `auth0.management_api_token` and `auth0.domain`.

- Make sure to adjust the `auth0-variables.js` (under `src/main/resources/static/`) to adjust `AUTH0_CLIENT_ID`, `AUTH0_DOMAIN` and `AUTH0_AUDIENCE`.

To **build** the project:

`./gradlew build`

To **run** the project:

`java -jar build/libs/app-0.0.1-SNAPSHOT.jar`

Open the your browser and load `http://localhost:8080`

User Guide:
===========

The Pizza42 allows the user to **login** or **sign up** with username/password or their Google account.
When not logged in, the user only sees the Pizza 42 logo and login button.

![Status Logged Out](docs/status_logged_out.png)

When logged in, he will see a **"refresh profile"**, **"logout"** and **"order"** button. 


**Login:**

![Status Login](docs/status_auth0.png)

After login, the user profile is loaded and displayed.
If the user's email address is verified, the order button is disabled and an info is displayed to the user. Otherwise the order button is enabled.

![Status Logged In](docs/status_logged_in.png)

**Refresh profile:**

In case the user meanwhile verified his email, he can use the "refresh profile" button to have the system check again. Another idea, not implemented though, is to implement an automatic recheck of the profile that checks via API request in certain intervals whether the profile is meanwhile validated.

**Order:**

When clicking the order button, the order is submitted and a status is returned from the server and displayed to the user. Usually, it should be successful.

![Status Ordered](docs/status_ordered.png)

**Logout:**

Logs the user out of the SPA.
