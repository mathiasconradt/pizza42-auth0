window.addEventListener('load', function() {
  var loadingSpinner = document.getElementById('loading');
  loadingSpinner.style.display = 'none';

  var userProfile;
  var requestedScopes = 'openid profile email';

  // auth0 API
  var webAuth = new auth0.WebAuth({
    domain: AUTH0_DOMAIN,
    clientID: AUTH0_CLIENT_ID,
    redirectUri: AUTH0_CALLBACK_URL,
    audience: AUTH0_AUDIENCE,
    responseType: 'token id_token',
    scope: requestedScopes,
    leeway: 60
  });

  // Buttons and event listeners
  var loginBtn = document.getElementById('btn-login');
  var logoutBtn = document.getElementById('btn-logout');
  var profileBtn = document.getElementById('btn-profile');
  var orderBtn = document.getElementById('btn-order');

  // Status divs and views
  var orderStatus = document.getElementById('order-status');
  var loginStatus = document.getElementById('login-status');
  var profileView = document.getElementById('profile-view');

  // Event listener for order button
  orderBtn.addEventListener('click', function() {
    callAPI('/api/order', true, 'POST', function(err, response) {
      if (err) {
        alert(err);
        return;
      }
      orderStatus.innerHTML = response.status;
    });
  });

  // Event listener for login button
  loginBtn.addEventListener('click', function login() {
    webAuth.authorize();
  });

  // Event listener for logout button
  logoutBtn.addEventListener('click',   function logout() {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('scopes');
    displayButtons();
  });

  // Event listener for profile button
  profileBtn.addEventListener('click', function() {
    getFullProfile();
  });

  // Setting auth result into client session / local storage
  function setSession(authResult) {

    // Set the time that the access token will expire at
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );

    // If there is a value on the `scope` param from the authResult,
    // use it to set scopes in the session for the user. Otherwise
    // use the scopes as requested. If no scopes were requested,
    // set it to nothing
    const scopes = authResult.scope || requestedScopes || '';

    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('scopes', JSON.stringify(scopes));
  }

  // Check if user is authenticated
  function isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    var isAuthenticated = new Date().getTime() < expiresAt;
    if (isAuthenticated) {
        getFullProfile();
    }
    return isAuthenticated;
  }

  // Button display logic
  function displayButtons() {
    orderBtn.style.display = 'none';
    orderStatus.innerHTML = '';
    if (isAuthenticated()) {
      loginBtn.style.display = 'none';
      logoutBtn.style.display = 'inline-block';
      orderBtn.style.display = 'inline-block';
      profileBtn.style.display = 'inline-block';
      loginStatus.innerHTML = 'You are logged in!';
    } else {
      loginBtn.style.display = 'inline-block';
      logoutBtn.style.display = 'none';
      profileBtn.style.display = 'none';
      orderBtn.style.display = 'none';
      loginStatus.innerHTML = 'You are not logged in! Please log in to continue.';
      profileView.innerHTML = '';
    }

  }

  // Loads full user profile from API backend based on user's access token
  function getFullProfile() {

    loadingSpinner.style.display = 'inline-block';

    callAPI('/api/profile', true, 'GET', function(err, response) {

        if (err || response==undefined) {
          loadingSpinner.style.display = 'none';
          logoutBtn.click();
          return;
        }

        // Fetch gender, name and picture from user profile root, but check if there is additional information
        // in an identities profile data available, which would then have a higher priority.

        // gender & title
        var gender;
        if (response!==undefined) {
            gender = response.gender;
            if (gender===undefined || gender === null && response!==undefined && response.identities!==undefined) {
                for (var i=0; i<response.identities.length; i++) {
                    if (response.identities[i].profileData && response.identities[i].profileData.gender) {
                        gender = response.identities[i].profileData.gender;
                    }
                }
            }
        }
        var title = '';
        if (gender==='male') title = 'Mr.';
        else if (gender==='female') title = 'Ms.';

        // name
        var name = '';
        if (response!==undefined) {
            name = response.name;
            if (response.identities!==undefined) {
                for (var i=0; i<response.identities.length; i++) {
                    if (response.identities[i].profileData && response.identities[i].profileData.name) {
                        name = response.identities[i].profileData.name;
                    }
                }
            }
        }

        // picture
        var picture;
        if (response!==undefined) {
            picture = response.picture;
            if (response.identities!==undefined) {
                for (var i=0; i<response.identities.length; i++) {
                    if (response.identities[i].profileData && response.identities[i].profileData.picture) {
                        picture = response.identities[i].profileData.picture;
                    }
                }
            }
        }

        // generate user profile html snippet
        var profileHtml = 'Name: '+ title + ' ' + name + '<br/>' + 'Email: ' + response.email + ' (' + (response.email_verified ? 'verified' : 'not verified') + ')'
            + '<br/><img width="200" src="' + picture + '"/>';

        // show or hide order button based on email verification status
        if (response.email_verified) {
            orderBtn.disabled = false;
            orderStatus.innerHTML = '';
        } else {
            orderBtn.disabled = true;
            orderStatus.innerHTML = 'Your email address must be verified to be able to order.<br/>If you already verified your email address, please refresh the profile.';
        }

        loadingSpinner.style.display = 'none';

        // update message
        profileView.innerHTML = profileHtml;

    });
  }

  // Handle authentication, check if user is already authenticated or not
  function handleAuthentication() {
    webAuth.parseHash(function(err, authResult) {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        setSession(authResult);
        loginBtn.style.display = 'none';
      } else if (err) {
        console.log(err);
        alert(
          'Error: ' + err.error + '. Check the console for further details.'
        );
      }
      displayButtons();
    });
  }

  // Generic helper method to call API backend
  function callAPI(endpoint, secured, method, cb) {
    var url = API_URL + endpoint;
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    if (secured) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
    }
    xhr.onload = function() {
      if (xhr.status == 200) {
        var message = JSON.parse(xhr.responseText);
        if (cb!==undefined) cb(null, message);
      } else {
        if (cb!==undefined) cb(xhr.statusText);
      }
    };
    xhr.send();
  }

  // Initial method calls when page is loaded
  handleAuthentication();
});
