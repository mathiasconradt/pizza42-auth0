package com.pizza42.app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Single page application controller. It's only purpose is to deliver the static SPA page (index.html).
 */
@RestController
public class SinglePageController {

    @RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

}
