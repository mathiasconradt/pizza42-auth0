package com.pizza42.app;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.interfaces.RSAPublicKey;
import java.util.Calendar;
import java.util.stream.Collectors;

/**
 * Backend API controller for Pizza 42.
 */
@EnableConfigurationProperties
@RestController
@Component
public class ApiController {

    @Autowired
    private Environment env;

    /**
     * API to fetch the full user profile via Auth0 management API.
     *
     * @param request Request
     * @return Full user profile as JSON string
     * @throws Exception
     */
    @RequestMapping(value = "/api/profile", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public String profile(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Verify the access token via JWT library.
        // (This was my first implementation before using JwtWebSecurityConfigurer)
        ObjectMapper mapper = new ObjectMapper();
        DecodedJWT jwt;
        try {
            jwt = verifyToken(request.getHeader("Authorization").replace("Bearer ", "").trim());
        } catch (JWTVerificationException exception) {
            exception.printStackTrace(); // Invalid signature/claims
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return "{}";
        }


        // Extract the sub from access token.
        String decodedPayloadString = new String(Base64Utils.decodeFromString(jwt.getPayload()));
        // Get full user profile via management api, using the sub/user_id we got from above.
        JsonNode sub = mapper.readTree(decodedPayloadString).get("sub");
        String fullUserProfileString = makeHttpRequest(
            "https://" + env.getProperty("auth0.domain") + "/api/v2/users/" + sub.textValue(),
            "Bearer " + env.getProperty("auth0.management_api_token"));
        // store user profile in session
        mapper = new ObjectMapper();
        JsonNode fullUserProfileObj = mapper.readTree(fullUserProfileString);


        // Fetch google connections from Google People API.
        //
        // If a Google access token exists and we did not yet fetch the number of connections, fetch it now.
        //
        // First, extract the Google access token from the user profile.
        String googleToken = null;
        boolean googleConnectionsAlreadyFetched = false;
        JsonNode identities = fullUserProfileObj.get("identities");

        if (identities != null && identities.isArray()) {
            for (JsonNode identity : identities) {
                if (identity.get("provider").textValue().equals("google-oauth2") && !identity.get("access_token").isNull()) {
                    googleToken = identity.get("access_token").textValue();
                }
                if (fullUserProfileObj.get("user_metadata") != null) {
                    JsonNode googleConnectionsObj = fullUserProfileObj.get("user_metadata").get("googleConnections");
                    if (googleConnectionsObj != null && googleConnectionsObj.isInt())
                        googleConnectionsAlreadyFetched = true;
                }
            }
        }

        // Note that we only fetch the connections at first time the user authenticates using his Google account,
        // but not afterwards anymore. Therefore we don't need to handle refresh tokens.
        // Otherwise, this would be needed due to the expiration of the access token at some point.
        if (!googleConnectionsAlreadyFetched && googleToken != null) {
            try {
                String userConnectionsString = makeHttpRequest(
                    "https://people.googleapis.com//v1/people/me/connections?personFields=names",
                    "Bearer " + googleToken);
                JsonNode userConnectionsObj = mapper.readTree(userConnectionsString);
                int userConnectionsCount = userConnectionsObj.get("totalPeople").intValue();

                // write to user metadata
                String userMetaDataUpdate = "{ \"user_metadata\" : { \"googleConnections\": " + userConnectionsCount + " } }";

                // HttpURLConnection and URLConnection does not support the PATCH method by default, so we need to
                // fix it using reflection.
                // https://stackoverflow.com/questions/22355235/patch-request-using-jersey-client/39641592#39641592
                try {
                    Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
                    methodsField.setAccessible(true);
                    // get the methods field modifiers
                    Field modifiersField = Field.class.getDeclaredField("modifiers");
                    // bypass the "private" modifier
                    modifiersField.setAccessible(true);
                    // remove the "final" modifier
                    modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);
                    /* valid HTTP methods */
                    String[] methods = {
                        "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
                    };
                    // set the new methods - including patch
                    methodsField.set(null, methods);
                } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
                    e.printStackTrace();
                }

                URL url = new URL("https://" + env.getProperty("auth0.domain") + "/api/v2/users/" + sub.textValue());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("PATCH");
                conn.setRequestProperty("Authorization", "Bearer " + env.getProperty("auth0.management_api_token"));
                conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                conn.setRequestProperty("Content-Length", String.valueOf(userMetaDataUpdate.getBytes().length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(userMetaDataUpdate.getBytes());
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line; (line = reader.readLine()) != null; ) {
                    builder.append(line).append("\n");
                }
                reader.close();
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace(); // might be a 401 error when the token has expired.
            }
        }
        return fullUserProfileString;
    }


    /**
     * Submit order (dummy method) and returns a status message back to the client.
     * This method checks the user's email address verification status and only allows the order
     * submission if the email address is validated.
     *
     * @param request Request
     * @return Status message regarding the success of order submission.
     * @throws Exception
     */
    @RequestMapping(value = "/api/order", method = RequestMethod.POST)
    public String order(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Verify the access token via JWT library.
        // (This was my first implementation before using JwtWebSecurityConfigurer)
        ObjectMapper mapper = new ObjectMapper();
        DecodedJWT jwt;
        try {
            jwt = verifyToken(request.getHeader("Authorization").replace("Bearer ", "").trim());

            // TODO We could work with scopes here, such as "create:orders", but since every user should be able to order (verified email required)
            // and this app basically only has this one function, I just used this simple check regarding the email verification here.
            String decodedPayloadString = new String(Base64Utils.decodeFromString(jwt.getPayload()));
            JsonNode scopes = mapper.readTree(decodedPayloadString).get("scope");
            // if (!scopes.textValue().contains("create:orders")) {...}

        } catch (JWTVerificationException exception) {
            exception.printStackTrace(); // Invalid signature/claims
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return "{}";
        }

        // Get user profile via access token on behalf of the user, in order to fetch the email_verified flag
        // as well as any user data that is needed to process the order.
        String userProfileString = makeHttpRequest(
            "https://" + env.getProperty("auth0.domain") + "/userinfo",
            request.getHeader("Authorization"));
        JsonNode userProfileObj = mapper.readTree(userProfileString);

        if (userProfileObj == null) {
            return "{\"status\": \"Access denied\"}";
        } else if (!userProfileObj.get("email_verified").asBoolean()) {
            return "{\"status\": \"Email not verified. Cannot place order.\"}";
        } else if (userProfileObj.get("email_verified").asBoolean()) {
            return "{\"status\": \"Success. Order has been placed.\"}";
        }
        return "{\"status\": \"Unknown Error\"}";
    }


    /**
     * Verify an access token via JWT.io library.
     *
     * @param token Access token
     * @return Decoded JWT
     * @throws Exception
     */
    public DecodedJWT verifyToken(String token) throws Exception {

        final String issuer = "https://" + env.getProperty("auth0.domain");
        final String audience = env.getProperty("auth0.api_audience");

        // Extract the kid from access token.
        DecodedJWT jwtJson = JWT.decode(token);
        ObjectMapper mapper = new ObjectMapper();
        String decodedHeaderString = new String(Base64Utils.decodeFromString(jwtJson.getHeader()));
        JsonNode kid = mapper.readTree(decodedHeaderString).get("kid");

        // Fetch the public key.
        JwkProvider provider = new UrlJwkProvider(issuer);
        Jwk jwk = provider.get(kid.textValue());

        // Verify the token.
        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
        JWTVerifier verifier = JWT.require(algorithm)
            .withIssuer(issuer + "/")
            .withAudience(audience)
            .acceptExpiresAt(Calendar.getInstance().getTimeInMillis())
            .build();
        DecodedJWT jwt = verifier.verify(token);
        return jwt;
    }

    /**
     * Make a GET http request to given url with given authorization header.
     *
     * @param urlString
     * @param authorizationHeader
     * @return
     * @throws IOException
     */
    public String makeHttpRequest(String urlString, String authorizationHeader) throws IOException {
        URL url = new URL(urlString);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        if (authorizationHeader != null) urlConnection.setRequestProperty("Authorization", authorizationHeader);
        urlConnection.connect();
        InputStream inputStream = urlConnection.getInputStream();
        return streamToString(inputStream);
    }

    /**
     * Convert input stream to string.
     *
     * @param inputStream Input stream
     * @return String
     */
    public String streamToString(final InputStream inputStream) {
        final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        return br.lines().parallel().collect(Collectors.joining("\n"));
    }

}
